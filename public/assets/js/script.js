const canvas = document.getElementById("the_canvas");    //to be able draw on canvas
const context = canvas.getContext("2d");

let clockOutput = document.getElementById("time");

const sound = new Audio("assets/music/song.wav");
const speed = 5;

                                                                                            //DOESN'T WORK COUNTING SYSTEM, IF I WANT TO CONTINUE COUNTING

let gameStatus = new Boolean(true);

let direction = 0;      //0 - down, 1- up, 2- right, 3 - left
let frames = 4;         //i've 4 frames in a row for character
let currentFrame = 0;   //for player animation
let playerX = 10;       //position of player (by default)
let playerY = 100;


let heartFrames = 8;    //frames for heart animation
let currentHeartFrame = 0;  
let heartX  = 200;      //position of heart
let heartY  = 200;


let starFrames = 7;     //frames for star animation
let currentStarFrame = 0;
let starX = 300;        //position of star
let starY = 300;


let donutFrames = 3;    //frames for donut animation
let currentDonutFrame = 0;
let donutX = 350;       //position of donut
let donutY = 350;



const username = localStorage.getItem('username');
console.log(username);
let score = localStorage.getItem('score');

score = parseInt(score);    //understand as int, so it doesn't add numbers, but instead sum them up

let scoreCounter = 0;  //player score counter

if (score)
{
    scoreCounter = score;
}


//-------------------------------TIME CLOCK--------------------------------------------------------------//
function updateTime()
{
    context.fillStyle = 'white';
    
    let date  = new Date();
    let formattedDataClock =  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    let formattedData = date.getDate() + "/" + ( date.getMonth() + 1) + "/" + date.getFullYear();

    context.font = '600 20px Arial';
    context.fillText(formattedDataClock, 30, 35);
    context.fillText(formattedData, 30, 60);

    // Dashed TOP line around clock
    context.strokeStyle = 'rgba(255, 255, 255, 0.5)';
    context.lineWidth = 2;  //if I've by default 1, then colour isn't that bright

    context.beginPath();
    context.setLineDash([5, 5]);
    context.moveTo(0, 1);
    context.lineTo(140, 1);
    context.stroke();

    // Dashed LEFT line around clock
    context.beginPath();
    context.setLineDash([5, 5]);
    context.moveTo(1, 0);
    context.lineTo(1, 80);
    context.stroke();

    // Dashed BOTTOM line around clock
    context.beginPath();
    context.setLineDash([5, 5]);
    context.moveTo(0, 80);
    context.lineTo(140, 80);
    context.stroke();

    // Dashed RIGHT line around clock
    context.beginPath();
    context.setLineDash([5, 5]);
    context.moveTo(140, 1);
    context.lineTo(140, 80);
    context.stroke();
   
    //TEXT MESSAGE                                                          
    context.font = '20px Courier New';
    context.fillStyle = 'black';
    context.fillText("Press M to play music", 160, 20);    
    context.fillText("Press P to pause music", 160, 40);    
    context.fillText("Press Esc to exit full screen", 160, 60);   
    context.fillText("Click on the button to open page in fullscreen", 160, 80);  
}


//------------------------------CHARACTER---------------------------------------//
function GamerInput(input)
{
    this.action = input;
}

let gamerInput = new GamerInput("None");


function GameObjects(spritesheet, x, y, width, height)
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let sprite = new Image();   //it makes java script aware that it is image object, set image
sprite.src = "assets/img/Player_Sprites.png"; //image location
let player = new GameObjects(sprite, playerX, playerY, 64, 64);

let heartSprite = new Image();
heartSprite.src = "assets/img/heart.png";
let heart = new GameObjects(heartSprite, heartX, heartY, 11, 10);

let starSprite = new Image();
starSprite.src = "assets/img/star.png";
let star = new GameObjects(starSprite, starX, starY, 11, 10);

let donutSprite = new Image();
donutSprite.src = "assets/img/donut.png";
let donut = new GameObjects(donutSprite, donutX, donutY, 28, 23);



function draw()
{
    animate();
    drawHealthbar();
    updateTime(); 

    if(gameStatus)
    {
        collision();
    }

    displayScore();

    if (fillVal >= 0.99)    //to cover all gameplay and clock
    {
        gameOver();
    }
}



let initial = new Date().getTime(); 
console.log(initial);
let current;

function animate()
{
    current = new Date().getTime();
    if (current - initial >= 200)  //how much time i want to collapse frames, every 200 milisec, basically, we allow frame increments to change
    {
        currentFrame = (currentFrame + 1) % frames;     //changing current frame
        currentHeartFrame = (currentHeartFrame + 1) % heartFrames;
        currentStarFrame = (currentStarFrame + 1) % starFrames;
        currentDonutFrame = (currentDonutFrame + 1) % donutFrames;

        initial = current;      //reset initial
    }

    context.clearRect(0, 0, canvas.width, canvas.height); //that it won't be visible on the screen all layers of player
    context.drawImage(sprite, (sprite.width / frames) * currentFrame, direction * 64, 64, 64, player.x, player.y, 64, 64);  //want to draw an image on canvas, position by deafult  
    
    context.drawImage(heartSprite, (heartSprite.width / heartFrames) * currentHeartFrame, 0, 11, 10, heart.x, heart.y, 22, 20);
    context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 11, 10, star.x, star.y, 22, 20);
    context.drawImage(donutSprite, (donutSprite.width / donutFrames) * currentDonutFrame, 0, 28, 23, donut.x, donut.y, 28, 23);
}


function input(event) //player interaction with (keyboard) character (event listener)
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            

            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            

            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key

            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            
            case 77: // m - music
                gamerInput = new GamerInput("Music");
                break;

            case 80: //p - pause music
                gamerInput = new GamerInput("Pause");
                break;

            default:
                gamerInput = new GamerInput("None");    //if player is not interacting
        }
    }

    else
    {
        gamerInput = new GamerInput("None");
    }

    //console.log("Gamer Input :" + gamerInput.action);
}

function update()   //to make character moving
{

    if (player.y <= 0)          /////  UP  /////
    {
        //do nothing
    }

    else 
    {
        if (gamerInput.action === "Up")
        {
            direction = 1;
            
            if (player.x <= 140 && player.y <= 77)
            {
                player.y = player.y;                
            }
            
            else
            {
                player.y -= speed;
            }
          
        }
    }

    
    if (player.y >= 415)        /////  DOWN  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Down")
        {
            direction = 0;
            player.y += speed;
        }
    }

  
    if (player.x <= 0)          /////  LEFT  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Left")
        {
            direction = 3;

            if (player.x <= 140 && player.y <= 77)
            {
                player.x = player.x;                
            }

            else
            {
                player.x -= speed;
            }
            
        }
    }


    if (player.x >= 1035)          /////  RIGHT  /////
    {
        //do nothing
    }

    else
    {
        if (gamerInput.action === "Right")
        {
            direction = 2;
            player.x += speed;
        }
    }
}


//------------------------------------BUTTONS FOR FULLSCREEN MODE------------------------------------------//


/* Get the element you want displayed in fullscreen */ 
var element = document.documentElement;

/* Function to open fullscreen mode */
function openFullscreen() 
{
  if (element.requestFullscreen) 
  {
    element.requestFullscreen();
  } 
  
  else if (element.mozRequestFullScreen) 
  { /* Firefox */
    element.mozRequestFullScreen();
  } 
  
  else if (element.webkitRequestFullscreen) 
  { /* Chrome, Safari & Opera */
    element.webkitRequestFullscreen();
  } 
  
  else if (element.msRequestFullscreen) 
  { /* IE/Edge */
    element = window.top.document.body; //To break out of frame in IE
    element.msRequestFullscreen();
  }
}


/* Function to close fullscreen mode */
function closeFullscreen() 
{
    if (document.exitFullscreen)        /* Code examples are taken from w3schools.com */
    {
      document.exitFullscreen();
    } 
    
    else if (document.mozCancelFullScreen) 
    {
      document.mozCancelFullScreen();
    } 
    
    else if (document.webkitExitFullscreen) 
    {
      document.webkitExitFullscreen();
    } 
    
    else if (document.msExitFullscreen) 
    {
      window.top.document.msExitFullscreen();
    }
}

// Events for button
var output = document.getElementById("the_canvas");

document.addEventListener("fullscreenchange", function() 
{
  output.innerHTML = "fullscreenchange event fired!";
});

document.addEventListener("mozfullscreenchange", function() 
{
  output.innerHTML = "mozfullscreenchange event fired!";
});

document.addEventListener("webkitfullscreenchange", function() 
{
  output.innerHTML = "webkitfullscreenchange event fired!";
});

document.addEventListener("msfullscreenchange", function() 
{
  output.innerHTML = "msfullscreenchange event fired!";
});



//------------------------------------RANDOM POSITION-----------------------------------------//

let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);


function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}


function donutPos()
{
    donut.x = Math.floor(Math.random() * 1050); 
    donut.y = Math.floor((Math.random() * 340) + 120); 
}

//setInterval(donutPos, 5000);

let counter = 0;
let seconds = 0;

function collision()  //put in draw function
{
    counter++;

    if (counter >= 143)
    {
        seconds++;
        counter = 0;
    }

    if (seconds >= 4)
    {
        seconds = 0;
        donutPos();
    }
    
    if (player.x < donut.x + donut.width && player.x + player.width > donut.x && player.y < donut.y && player.y + player.height > donut.y ) 
    {
        context.drawImage(donutSprite, (donutSprite.width / donutFrames) * currentDonutFrame, 0, 28, 23, donut.x, donut.y, 28, 23);
        console.log("collision!");

        scoreCounter = scoreCounter + 15;
        localStorage.setItem("score", scoreCounter);

        seconds = 0; 
        donutPos();
    }  


    if (player.x < star.x + star.width && player.x + player.width > star.x && player.y < star.y && player.y + player.height > star.y ) 
    {
        context.drawImage(starSprite, (starSprite.width / starFrames) * currentStarFrame, 0, 11, 10, star.x, star.y, 22, 20);
        star.x = Math.floor(Math.random() * 1050);
        star.y = Math.floor((Math.random() * 340) + 120);   //140

        scoreCounter = scoreCounter + 5;
        localStorage.setItem("score", scoreCounter);
    }

    if (player.x < heart.x + heart.width && player.x + player.width > heart.x && player.y < heart.y && player.y + player.height > heart.y ) 
    {
        context.drawImage(heartSprite, (heartSprite.width / heartFrames) * currentHeartFrame, 0, 11, 10, heart.x, heart.y, 22, 20);
        heart.x = Math.floor(Math.random() * 1050);
        heart.y = Math.floor((Math.random() * 340) + 120); 

        scoreCounter = scoreCounter + 10;
        localStorage.setItem("score", scoreCounter);
    }
}

//--------------------------------SCORE TEXT TO DISPLAY ON SCREEN--------------------------------------------//

function displayScore()
{
    let scoreString = "score: " + scoreCounter;
    console.log("score" + scoreCounter);
    context.font = '22px monospace';
    context.fillText(scoreString, 950, 25);
}


//--------------------------------the working of a mouse-based joystick-------------------------------------//

    let static = nipplejs.create({
        zone: document.getElementById('joystick'),
        mode: 'static',
        position: {left: '50%', top: '50%'},
        color: 'blue'
    });


    //nipple.on('start move end dir plain', function (evt) {
    static.on('dir:up', function (evt, data) 
    {
        gamerInput = new GamerInput("Up");
    });
    static.on('dir:down', function (evt, data) 
    {
        gamerInput = new GamerInput("Down");
    });
    static.on('dir:left', function (evt, data) 
    {
        gamerInput = new GamerInput("Left");
    });
    static.on('dir:right', function (evt, data) 
    {
        gamerInput = new GamerInput("Right");
    });
    static.on('end', function (evt, data)
    {
        gamerInput = new GamerInput("None");
    });

//------------------------------------------------------------------------------//

// Draw a HealthBar on Canvas, can be used to indicate players health
var fillVal = 0;
function drawHealthbar() 
{
    var width = 1100;
    var height = 20;
  
    // Draw the background
    context.fillStyle = "#000000";
    context.clearRect(0, 480, canvas.width, canvas.height);
    context.fillRect(0, 480, width, height);
  
    // Draw the fill
    context.fillStyle = "#F09EFF";
    context.fillRect(0, 480, fillVal * width, height);
    fillVal += 0.0005;  //timer


    if (fillVal >= 0.99)    //when time value is bigger than 0.99, then draws over black screen with message "Game Over"
    {
        gameOver();
    }
}




function gameOver()
{
    context.fillStyle = 'black';
    context.fillRect(0,0,1100,500);     //x,y,width,height of the canvas
    context.font = '120px Courier New';
    context.fillStyle = 'white';
    context.fillText("GAME OVER", 200, 250);    

    context.fillText("Score: " + scoreCounter, 200, 400);

    static.destroy();   //to make joystick dissapear after game is over, so player can't move and earn points even after game is finished
    gameStatus = false;
}

//-----------------------D-PAD BUTTONS--------------------------------------

function noMoving()
{
    gamerInput = new GamerInput("None");
}

function pressUp()
{
    gamerInput = new GamerInput("Up");
}

function pressDown()
{
    gamerInput = new GamerInput("Down");
}

function pressLeft()
{
    gamerInput = new GamerInput("Left");
}

function pressRight()
{
    gamerInput = new GamerInput("Right");
}

//------------------------------------------------------------------------------


function gameloop()
{
    update();   //process the user inputs and update the position vars
    draw();     //use the updated values and actually draw the images on screen

    window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);


window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('keydown', event => 
{   
    if (gamerInput.action === "Music")
    {
        sound.play();    /* the audio is now playable; play it if permissions allow */   
    }
   
    if (gamerInput.action === "Pause")  //will pause music only if player press p button down
    {
        sound.pause();
    }
});